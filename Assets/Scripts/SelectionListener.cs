using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*
 * If your class implements this interface, it can be added to SelectionHandler as
 * selection listener
 */
public interface SelectionListener
{
	/*
	 * Called by SelectionHandler when in-game selection has changed
	 */
	void selectionChanged (IList<GameSelectable> selection);
}

