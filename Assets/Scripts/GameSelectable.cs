using System;
using System.Collections.Generic;
using UnityEngine;

public interface GameSelectable
{

	/*
	 * Is called by selection handler, when the object gets selected
	 */
	void select ();

	/*
	 * Is called by selection handler, when the object loses selection
	 */
	void unselect ();
}