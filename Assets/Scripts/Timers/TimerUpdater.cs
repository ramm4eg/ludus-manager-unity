﻿using UnityEngine;
using System.Collections;

/*
 * A small script, that tells TimerUtil how and when to update it's timers
 */
public class TimerUpdater : MonoBehaviour {
	
	/*
	 * Is called once per physics calculation. Tells all instances of TimerUtil 
	 * how much time has passed since last physics calculation, so they can update
	 * their timers.
	 */
	void FixedUpdate () {
		TimerUtil.updateAllTimers (Time.fixedDeltaTime);
	}
}
