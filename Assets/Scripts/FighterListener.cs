using UnityEngine;
using System.Collections;

/*
 * A listener, that can be added to fighter (FighterController.addListener() or
 * Fighter.addListener()).
 * Get's called, when something changes in fighter's stats.
 */
public interface FighterListener
{
	/*
	 * Is called directly after fighter's offence has changed
	 * 
	 * @param oldValue old offence value
	 * @param newValue new offence value
	 */
	void offenseChanged (int oldValue, int newValue);

	/*
	 * Is called directly after fighter's defence has changed
	 * 
	 * @param oldValue old defence value
	 * @param newValue new defence value
	 */
	void defenseChanged (int oldValue, int newValue);

	/*
	 * Is called directly after fighter's swiftness has changed
	 * 
	 * @param oldValue old swiftness value
	 * @param newValue new swiftness value
	 */
	void swiftnessChanged (int oldValue, int newValue);

	/*
	 * Is called directly after the fighter has gone out (dead, maimed, unconscious etc.)
	 */
	void knockedOut ();
}