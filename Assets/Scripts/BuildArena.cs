﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Fighters;

/*
 * This class manages the preparation of the arena scene:
 * creates the teams, places the fighters onto the arena, initializes the HUD.
 */
public class BuildArena : MonoBehaviour
{
	public int numFightersPerTeam;
	public GameObject fighter;

	/*
	 * Initialize arena scene
	 */
	void Start ()
	{
		CreateTeams ();
	}

	/*
	 * Create teams, fill them with fighters, place fighters to the arena and 
	 * initialize the HUD
	 */
	private void CreateTeams ()
	{
		// prepare the HUD controller
		HudController hud = HudController.instance;

		Team team1 = new Team (true);
		for (int i = 0; i < numFightersPerTeam; i++) {
			GameObject fighterInst = Instantiate (fighter, new Vector3 (-1.5f, 0f, i), Quaternion.identity) as GameObject;
			fighterInst.transform.Rotate (new Vector3 (0f, 90f, 0f));
			FighterController contr = fighterInst.GetComponent<FighterController> ();

			string fighterName = NameGenerator.generateName ();
			Fighter data = new Fighter (fighterName, team1);

			contr.setData (data);

			// the skill must be provided with a controller, so it can tell the controller
			// to interupt the fighter's actions if needed
			RetreatSkill retreat = new RetreatSkill (contr);
			contr.addSkill (retreat);

			hud.displayFighter (contr);
		}

		Team team2 = new Team (false);
		for (int i = 0; i < numFightersPerTeam; i++) {
			GameObject fighterInst = Instantiate (fighter, new Vector3 (1.5f, 0f, i), Quaternion.identity) as GameObject;
			fighterInst.transform.Rotate (new Vector3 (0f, -90f, 0f));
			FighterController contr = fighterInst.GetComponent<FighterController> ();

			string fighterName = NameGenerator.generateName ();
			Fighter data = new Fighter (fighterName, team2);
			
			contr.setData (data);

			// the skill must be provided with a controller, so it can tell the controller
			// to interupt the fighter's actions if needed
			RetreatSkill retreat = new RetreatSkill (contr);
			contr.addSkill (retreat);
			
			hud.displayFighter (contr);
		}

	}
}
