using UnityEngine;
using System.Collections;

/*
 * Attack and damage calculator. 
 */
public class AttackManager
{

	/*
	 * Types of damage, that can be done to a fighter.
	 */ 
	public enum DamageType
	{
		OFFENSE,
		DEFENSE,
		SWIFTNESS
	}

	private static AttackManager instance = null;

	/*
	 * Singleton
	 */
	public static AttackManager getInstance ()
	{
		if (instance == null) {
			instance = new AttackManager ();
		}
		return instance;
	}

	/*
	 * Hidden constructor
	 */
	private AttackManager ()
	{
	}

	/*
	 * Calculate and apply damage from attacker to defender.
	 * 
	 * @param attacker attacking fighter
	 * @param defender target of the attack 
	 */
	public void calculateAttack (FighterController attacker, FighterController defender)
	{
		DamageType damage = attacker.getPreferedDamageType ();
		int defense = defender.getDefense ();
		var offense = attacker.getOffense ();
		int maxRoll = offense + defense;
		int roll = Random.Range (1, maxRoll + 1);

		if (roll <= defense * 0.5f) {
			// retaliation
			defender.retaliate ();
		} else if (roll <= defense * 1.5f) {
			// successful defense
		} else if (roll <= defense * 2) {
			// minor damage
			doDamage (defender, damage, 1);
		} else if (roll == maxRoll) {
			// kritical damage (maim or kill)
			defender.knockOut ();
			return;
		} else {
			// major damage
			doDamage (defender, damage, (roll / defense) - 1);
		}
	}

	/*
	 * Apply damage to the target
	 * 
	 * @param defender target
	 * @param damageType attack, defence or swiftness
	 * @param amount by how much it should be decreased
	 */
	private void doDamage (FighterController defender, DamageType damageType, int amount)
	{
		switch (damageType) {
		case DamageType.DEFENSE:
			int defense = defender.getDefense ();
			defense -= amount;
			defender.setDefense (defense);
			if (defense <= 0) {
				defender.knockOut ();
			}
			break;
		case DamageType.OFFENSE:
			int offense = defender.getOffense ();
			offense -= amount;
			defender.setOffense (offense);
			if (offense <= 0) {
				defender.knockOut ();
			}
			break;
		case DamageType.SWIFTNESS:
			int swiftness = defender.getSwiftness ();
			swiftness -= amount;
			defender.setSwiftness (swiftness);
			if (swiftness <= 0) {
				defender.knockOut ();
			}
			break;
		default:
			break;
		}
	}
}
