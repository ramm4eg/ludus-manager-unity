using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

/*
 * Utility to set timers and trigger timer listeners 
 */
class TimerUtil
{
	private static Dictionary<TimerOwner, TimerUtil> instances = new Dictionary<TimerOwner, TimerUtil> ();
	private TimerOwner owner;
	private Dictionary<string, GameTimer> timers;

	/*
	 * There is only one instance of this utility per timer owner
	 */
	public static TimerUtil getInstance (TimerOwner owner)
	{
		TimerUtil instance = null;
		bool found = instances.TryGetValue (owner, out instance);
		if (!found) {
			instance = new TimerUtil (owner);
			instances.Add (owner, instance);
		}
		return instance;
	}

	/*
	 * Hidden constructor. Please use getInstance(owner) instead.
	 */
	private TimerUtil (TimerOwner owner)
	{
		this.owner = owner;
		timers = new Dictionary<string, GameTimer> ();
	}

	/*
	 * Updates the time on all timers in all instances of this class. Is called 
	 * once per physics calculation by TimeUpdater.
	 */
	public static void updateAllTimers (float fixedDeltaTime)
	{
		foreach (TimerUtil instance in instances.Values) {
			instance.updateTimers (fixedDeltaTime);
		}
	}

	/*
	 * Set a timer's time to provided value. If the timer with provided ID does
	 * not exist yet, create it.
	 * 
	 * @param timerId ID of the timer
	 * @param time time value to set
	 */
	public void setTimer (string timerId, float time)
	{
		GameTimer timer = null;
		bool found = timers.TryGetValue (timerId, out timer);
		if (!found) {
			timer = new GameTimer (timerId);
			timers.Add (timerId, timer);
		}
		timer.setTime (time);
	}

	/*
	 * Update all timers of this instance. If one or more timers are expired,
	 * notify the owner.
	 */
	private void updateTimers (float fixedDeltaTime)
	{
		List<GameTimer> expired = new List<GameTimer> ();
		foreach (GameTimer timer in timers.Values) {
			bool ringing = timer.update (fixedDeltaTime);
			if (ringing) {
				expired.Add (timer);
			}
		}
		foreach (GameTimer timer in expired) {
			timers.Remove (timer.getId ());
			notifyListeners (timer.getId ());
		}
		expired.Clear ();
	}

	/*
	 * Notify the owner of this instance of TimerUtil, that one of the timers has
	 * expired.
	 */
	private void notifyListeners (string timerId)
	{
		owner.timerExpired (timerId);
	}
}


