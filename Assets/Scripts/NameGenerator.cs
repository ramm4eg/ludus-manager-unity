using UnityEngine;

/*
 * Utility to generate names for fighters.
 */
public class NameGenerator
{

	static bool[] used;
	static int usedCount = 0;
	private static string[] NAMES = {"Petrus", "Brutus", "Sclavus", "Magnus", "Verus", "Stabilis",
		"Romulus", "Remus", "Augustus", "Roman", "David", "Alex", "Michael", "Infesus",
		"Mors", "Valentulus", "Velox", "Serpens", "Draco", "Serpens", "Leo", "Canis", "Ursus",
		"Goliath", "Behemoth", "Leviathan"
	}; 

	/*
	 * Choose a random name from a list of allowed names. Does not give a 
	 * name twice, if possible.
 	 */
	public static string generateName ()
	{
		if (used == null) {
			init ();
		}

		if (usedCount == NAMES.Length) {
			init ();
		}

		int index = Random.Range (0, NAMES.Length);
		while (used[index]) {
			index = Random.Range (0, NAMES.Length);
		}
		used [index] = true;
		usedCount++;
		return NAMES [index];
	}

	/*
	 * (Re-) Initialize the utility. Forget, which names are already given.
	 */
	private static void init ()
	{
		used = new bool[NAMES.Length];
		for (int i = 0; i < used.Length; i++) {
			used [i] = false;
		}
		usedCount = 0;
	}
}

