﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

/*
 * Controller for the first (left-most) skill button. The button exists in the
 * scene independently from the skills of concrete fighters. Upon fighter 
 * selection this controller finds out, what skills the selected fighter has and
 * binds the first one of them to the button. On that point button gets visible
 * and active.
 */
public class SkillButton1Controller : MonoBehaviour, SelectionListener
{

	private static SkillButton1Controller instance;
	private Button button;
	private ActiveSkill skill = null;
	private Sprite sprite = null;

	/*
	 * Singleton. 
	 * Upon instantiation make button inactive. Button can not be inactive
	 * at the start of the arena, becaus otherwise this script will not be
	 * initialized.
	 */
	public void Start ()
	{
		if (instance != null) {
			Destroy (this);
			return;
		}

		instance = this;
		button = GetComponent<Button> ();
		SelectionHandler.getInstance ().addSelectionListener (this);
		button.gameObject.SetActive (false);
	}

	/*
	 * When button is clicked on, execute the assigned skill (if any)
	 */
	public void onClick ()
	{
		if (skill == null) {
			return;
		}
		skill.execute ();
	}

	/*
	 * Skill button controller binds the button to a skill depending on 
	 * current selection
	 */
	#region SelectionListener implementation
	public void selectionChanged (IList<GameSelectable> selection)
	{
		if (selection.Count != 1) {
			clearButton ();
		} else {
			GameSelectable selectable = selection [0];
			fillButton (selectable);
		}
	}
	#endregion

	/*
	 * Assign the button to no skill and make it inactive.
	 */
	private void clearButton ()
	{
		skill = null;
		sprite = null;
		button.gameObject.SetActive (false);
		button.image.sprite = sprite;
	}

	/*
	 * Evaluate selection. If it's a fighter, get his first skill and bind it
	 * to the skill button.
	 * The skill object has method execute(), which will be called, when the
	 * button is clicked on. Besides the skill object has a name of sprite 
	 * (image) representing the skill. This function finds the image amongst 
	 * assets and sets it to be shown on the button.
	 * 
	 * @param selection selected object
	 */
	void fillButton (GameSelectable selection)
	{
		if (selection is FighterController) {
			FighterController fighter = selection as FighterController;
			if (fighter.getTeam ().isPlayer ()) {
				skill = fighter.getFirstSkill ();
				if (skill != null) {
					string imageName = skill.getImageName ();
					Texture2D texture = Resources.Load (imageName) as Texture2D;
					Rect rectangle = new Rect (0, 0, 102, 102);
					Vector2 pivot = new Vector2 (0, 0);
					sprite = Sprite.Create (texture, rectangle, pivot);
					button.gameObject.SetActive (true);
					button.image.sprite = sprite;
				} else {
					clearButton ();
				}
			} else {
				clearButton ();
			}
		} else {
			clearButton ();
		}
	}


}
