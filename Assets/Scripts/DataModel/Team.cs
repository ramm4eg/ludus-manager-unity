using UnityEngine;
using System;

/*
 * Team of fighters
 */
public class Team
{
	private bool player = false;

	/*
	 * Constructor
	 * 
	 * @param isPlayer if true, the fighters of the team will be controlled
	 * 		by the player
	 */
	public Team (bool isPlayer)
	{
		player = isPlayer;
	}

	/*
	 * Is this team controlled by a player?
	 */
	public bool isPlayer ()
	{
		return player;
	}
}
