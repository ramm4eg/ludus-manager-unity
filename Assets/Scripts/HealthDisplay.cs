﻿using UnityEngine;
using System.Collections.Generic;

/*
 * Controller of fighter health display.
 * The health displays exist independently from fighters in the game HUD.
 * Upon creation of fighters they are assigned to health displays. After that
 * the health displays show the health states of assigned fighters.
 */
using UnityEngine.UI;


public class HealthDisplay: MonoBehaviour, FighterListener, SelectionListener
{

	FighterController fighter;
	const int RED_THRESHOLD = 5;
	const int ORANGE_THRESHOLD = 7;

	/*
	 * Assign a fighter to health display
	 */
	public void setFighter (FighterController f)
	{
		fighter = f;
		f.addListener (this);

		SelectionHandler.getInstance ().addSelectionListener (this);

		Text nameText = transform.FindChild ("Text").gameObject.GetComponent<Text> ();
		nameText.text = fighter.getName ();

//		// force initial health display update
//		offenseChanged (fighter, -1, fighter.getOffense ());
//		defenseChanged (fighter, -1, fighter.getDefense ());
//		swiftnessChanged (fighter, -1, fighter.getSwiftness ());
	}

	/*
	 * When a health display gets clicked on, the assigned fighter gets selected.
	 */
	public void OnClick ()
	{
		SelectionHandler.getInstance ().setSelection (new List<GameSelectable>{fighter});
	}

	/*
	 * TODO
	 * HUD is a fighter listener. Each time a fighter's health is changed, HUD
	 * controller updates the health display for that fighter.
	 */
	#region FighterListener implementation
	public void offenseChanged (int oldValue, int newValue)
	{
		// check if the health display must be updated
		if (hudMustBeUpdated (oldValue, newValue)) {
			GameObject fhd = gameObject;
			// turn off the old image
			if (oldValue > ORANGE_THRESHOLD) {
				fhd.transform.FindChild ("Img_Atc_Green").gameObject.SetActive (false);
			} else if (oldValue > RED_THRESHOLD) {
				fhd.transform.FindChild ("Img_Atc_Orange").gameObject.SetActive (false);
			} else {
				fhd.transform.FindChild ("Img_Atc_Red").gameObject.SetActive (false);
			}
				
			// turn on the new image
			if (newValue > ORANGE_THRESHOLD) {
				fhd.transform.FindChild ("Img_Atc_Green").gameObject.SetActive (true);
			} else if (newValue > RED_THRESHOLD) {
				fhd.transform.FindChild ("Img_Atc_Orange").gameObject.SetActive (true);
			} else {
				fhd.transform.FindChild ("Img_Atc_Red").gameObject.SetActive (true);
			}
		}
	}
	
	public void defenseChanged (int oldValue, int newValue)
	{
		// check if the health display must be updated
		if (hudMustBeUpdated (oldValue, newValue)) {
			GameObject fhd = gameObject;
			// turn off the old image
			if (oldValue > ORANGE_THRESHOLD) {
				fhd.transform.FindChild ("Img_Def_Green").gameObject.SetActive (false);
			} else if (oldValue > RED_THRESHOLD) {
				fhd.transform.FindChild ("Img_Def_Orange").gameObject.SetActive (false);
			} else {
				fhd.transform.FindChild ("Img_Def_Red").gameObject.SetActive (false);
			}
				
			// turn on the new image
			if (newValue > ORANGE_THRESHOLD) {
				fhd.transform.FindChild ("Img_Def_Green").gameObject.SetActive (true);
			} else if (newValue > RED_THRESHOLD) {
				fhd.transform.FindChild ("Img_Def_Orange").gameObject.SetActive (true);
			} else {
				fhd.transform.FindChild ("Img_Def_Red").gameObject.SetActive (true);
			}
		}
		
	}
	
	public void swiftnessChanged (int oldValue, int newValue)
	{
		// check if the health display must be updated
		if (hudMustBeUpdated (oldValue, newValue)) {
			GameObject fhd = gameObject;
			// turn off the old image
			if (oldValue > ORANGE_THRESHOLD) {
				fhd.transform.FindChild ("Img_Swi_Green").gameObject.SetActive (false);
			} else if (oldValue > RED_THRESHOLD) {
				fhd.transform.FindChild ("Img_Swi_Orange").gameObject.SetActive (false);
			} else {
				fhd.transform.FindChild ("Img_Swi_Red").gameObject.SetActive (false);
			}
				
			// turn on the new image
			if (newValue > ORANGE_THRESHOLD) {
				fhd.transform.FindChild ("Img_Swi_Green").gameObject.SetActive (true);
			} else if (newValue > RED_THRESHOLD) {
				fhd.transform.FindChild ("Img_Swi_Orange").gameObject.SetActive (true);
			} else {
				fhd.transform.FindChild ("Img_Swi_Red").gameObject.SetActive (true);
			}
		}
	}
	
	/*
	 * Check if the change in the health value must invoke the health display 
	 * update.
	 * There are three zones of health quality - red, orange and green. If the
	 * change of the health value doesn't make the fighter leave the zone he is
	 * already in, this function will return false.
	 */
	private bool hudMustBeUpdated (int oldValue, int newValue)
	{
		if (oldValue > ORANGE_THRESHOLD && newValue > ORANGE_THRESHOLD) {
			return false;
		} else if (oldValue > ORANGE_THRESHOLD && oldValue <= RED_THRESHOLD && newValue > ORANGE_THRESHOLD && newValue <= RED_THRESHOLD) {
			return false;
		} else if (oldValue <= RED_THRESHOLD && newValue <= RED_THRESHOLD) {
			return false;
		}
		return true;
	}
	
	public void knockedOut ()
	{
		GameObject fhd = gameObject;
		// turn on the "dead" image
		fhd.transform.FindChild ("Theta").gameObject.SetActive (true);
	}
	#endregion

	#region SelectionListener implementation
	public void selectionChanged (IList<GameSelectable> selection)
	{
		Image image = gameObject.GetComponent ("Image") as Image;
	
		// is the associated fighter one of the selected?
		bool selected = false;
		foreach (GameSelectable select in selection) {
			if (select == fighter) {
				selected = true;
			}
		}
		 
		if (selected) {
			// fill the background of the image
			image.fillCenter = true;
		} else {
			image.fillCenter = false;
		}
	}
	#endregion
}
