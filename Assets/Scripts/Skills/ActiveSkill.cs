/*
 * Abstract implementation of active skill
 */
public abstract class ActiveSkill
{
	protected string name;
	protected string imageName;

	/*
	 * Implement the skill execution here. This function will be called, when
	 * the user clicks on the skill button or when the skill trigger gets active.
	 */
	public abstract void execute();

	/*
	 * This function returns the name of the image, that is set for this skill. It 
	 * is called by button manager to find and display correct image for each skill.
	 */
	public string getImageName ()
	{
		return imageName;
	}
}

