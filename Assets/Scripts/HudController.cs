using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

/*
 * Controller of the HUD. 
 * Contains fighter -> health display map. Assigns fighters to health displays.
 * Updates health displays upon fighter changes.
 */ 
public class HudController : MonoBehaviour
{

	public GameObject[] fighterHealthDisplays;
	public GameObject[] enemyHealthDisplays;
	private int currentIndex = 0;
	private int currentEnemyIndex = 0;
	public static HudController instance;

	/*
	 * Singleton.
	 * Upon initialization make sure, there is only one instance of HUD controller
	 */
	void Start ()
	{
		if (instance != null) {
			Destroy (gameObject);
		}
		if (instance == null) {
			instance = this;
		}
	}

	/*
	 * Assign fighter to a health display and initialize that display.
	 */
	public void displayFighter (FighterController fighter)
	{
		GameObject fhd;
		if (fighter.getTeam ().isPlayer ()) {
			if (currentIndex >= fighterHealthDisplays.Length) {
				// no more possibilities to show fighter's health
				return;
			} 

			fhd = fighterHealthDisplays [currentIndex];
			currentIndex++;
		} else {
			if (currentEnemyIndex >= enemyHealthDisplays.Length) {
				// no more possibilities to show fighter's health
				return;
			}
			
			fhd = enemyHealthDisplays [currentEnemyIndex];
			currentEnemyIndex++;
		}

		fhd.SetActive (true);
		// bind health display to fighter
		fhd.GetComponent<HealthDisplay> ().setFighter (fighter);

	}
}
