using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Fighters;

/*
 * This class represents a fighter on the arena. It handles the fighters behaviour,
 * set's it animation, hold's the fighters stats and health state.
 */
public class FighterController : MonoBehaviour, GameSelectable, TimerOwner
{
	const int BASE_SWIFTNESS = 10;
	private Fighter data;
	private Transform target;
	private bool ignoreEnemies = false;
	private bool attackCoolDown = false;
	const float RETREAT_TIME = 1.0f;
	private List<ActiveSkill> activeSkills = new List<ActiveSkill> ();
	private PriorityMap priorities;

	FighterStateMachine stateMachine;

	/*
	 * Initialisation hook. Called, when the fighter is created in Unity.
	 */
	void Start ()
	{
		stateMachine = new FighterStateMachine (this);

		// set default priorities
		priorities = new PriorityMap ();
		priorities.setPriority (BehaviourKind.Aggression, 5);
		priorities.setPriority (BehaviourKind.Retreat, 1);
	}

	/*
	 * Add skill for fighter to use
	 */
	public void addSkill (ActiveSkill skill)
	{
		activeSkills.Add (skill);
	}

	/*
	 * Set data model to this fighter controller
	 */
	public void setData (Fighter d)
	{
		data = d;

		GameObject nameTag = transform.FindChild ("NameTag").gameObject as GameObject;
		GUIText nameTagText = nameTag.GetComponent<GUIText> () as GUIText;
		nameTagText.text = d.getName ();
	}

	public int getOffense ()
	{
		return data.getOffense ();
	}

	public void setOffense (int newValue)
	{
		data.setOffense (newValue);
	}

	public int getDefense ()
	{
		return data.getDefense ();
	}
	
	public void setDefense (int newValue)
	{
		data.setDefense (newValue);
	}

	public int getSwiftness ()
	{
		return data.getSwiftness ();
	}
	
	public void setSwiftness (int newValue)
	{
		data.setSwiftness (newValue);
	}

	
	/*
	 * Calculate fighter behaviour based on his state. 
	 * Called by Unity in each physics computation step.
	 */
	void FixedUpdate ()
	{
		if (!data.isKnockedOut ()) {
			stateMachine.act ();
		}
	}

	/*
	 * Establish the next action, the fighter will do depending on his
	 * behaviour priorities.
	 */
	public FighterActionState startNextAction ()
	{
		GetComponent<Animation> ().Play ("idlebattle");

		List<BehaviourKind> behaviours = priorities.getBehaviourWithHighestPriority ();
		int index = 0;
		if (behaviours.Count > 1) {
			index = Random.Range (0, behaviours.Count - 1);
		}
		BehaviourKind top = behaviours [index];

		switch (top) {
		case BehaviourKind.Aggression:
			// later this method will evaluate priorities of the fighter
			// for now it's just "run to nearest enemy and hit him"

			// find nearest enemy
			GameObject enemy = Utils.getInstance ().findNearestEnemy (this);
			if (enemy != null) {
				target = enemy.transform;
				if (isTargetInRange ()) {
					return FighterActionState.ATTACKING;
				} else {
					return FighterActionState.MOVING;
				}
			}
			break;
		case BehaviourKind.Retreat:
			GameObject retreatPoint = Utils.getInstance ().findNearestRetreatPoint (this);
			if (retreatPoint != null) {
				target = retreatPoint.transform;
				TimerUtil util = TimerUtil.getInstance (this);
				util.setTimer (RETREAT_TIMER_ID, RETREAT_TIME);
				ignoreEnemies = true;
				return FighterActionState.MOVING;
			} else {
				// retreating is not possible
				setPriority (BehaviourKind.Retreat, 0);
				return FighterActionState.NO_OP;
			}
			break;
		default:
			break;
		}

		// if no new action was started - think again in the next cycle
		return FighterActionState.NO_OP;

	}

	/*
	 * Change the position of the fighter, moving him towards the target (see field target)
	 * with the speed, that is defined by fighter's swiftness stat.
	 * This function must ONLY be called by the state machine! Don't use it anywhere else, please.
	 */
	public void move ()
	{
		if (isTargetAvailable ()) {
			float step = getSpeed () * Time.fixedDeltaTime;
			transform.LookAt (target);
			transform.position = Vector3.MoveTowards (transform.position, target.position, step);
			GetComponent<Animation> ().Play ("run");
		} else {
			interrupt ();
		}
	}

	/*
	 * Calculate the damage (if any), that a fighter does to his target and apply that damage.
	 * Trigger the attack animation.
	 * This function must ONLY be called by the state machine! Don't use it anywhere else, please.
	 */
	public void attack ()
	{
		if (!attackCoolDown) {
			if (!isTargetInRange ()) {
				interrupt ();
				return;
			}
			
			FighterController attackTarget = target.GetComponent<FighterController> () as FighterController;
			//Debug.Log(attackTarget);
			
			// do attack 
			AttackManager.getInstance ().calculateAttack (this, attackTarget);
			
			// change the animation state to attacking
			GetComponent<Animation> ().Play ("attack");
			transform.LookAt (target);
			
			// start cool down
			TimerUtil.getInstance (this).setTimer (ATTACK_COOLDOWN_TIMER_ID, getAttackTime ());
			attackCoolDown = true;
		} 
		
	}

	public Team getTeam ()
	{
		return data.getTeam ();
	}

	/*
	 * Is the fighter dead / maimed / unconscious?
	 */
	public bool isOut ()
	{
		return data.isKnockedOut ();
	}



	/*
	 * When the fighter collides with an enemy, he stops his movement.
	 */
	void OnCollisionEnter (Collision collision)
	{
		if (stateMachine.getState () == FighterActionState.MOVING) {
			if (!ignoreEnemies) {
				FighterController contr = collision.gameObject.GetComponent<FighterController> ();
				if (contr != null && 
					!Utils.getInstance ().isSameTeam (this, contr)) {
					interrupt ();
				}
			}
		}
	}

	/*
	 * Get the type of damage the fighter wants to do to his enemy:
	 * damage his offence, defence or swiftness.
	 */
	public AttackManager.DamageType getPreferedDamageType ()
	{
		int roll = Random.Range (1, 4);
		
		if (roll == 1) {
			return AttackManager.DamageType.DEFENSE;
		} else if (roll == 2) {
			return AttackManager.DamageType.OFFENSE;
		} else {
			return AttackManager.DamageType.SWIFTNESS;
		}
	}

	/*
	 * Knock out the fighter. This method must ONLY change the state of the fighter. 
	 * The state machine will call all other needed methods - animation, hud update etc.
	 */
	public void knockOut ()
	{
		stateMachine.stateToOut ();
	}

	/*
	 * The fighter retaliates, removing his attack cooldown
	 */
	public void retaliate ()
	{
		attackCoolDown = false;
	}

	public string getName ()
	{
		return data.getName ();
	}

	public void setName (string fName)
	{
		data.setName (fName);


	}

	/*
	 * Check if current target of the fighter is still a valid target in game 
	 * terms.
	 */
	public bool isTargetAvailable ()
	{
		return Utils.getInstance ().isTargetAvailable (target.gameObject);
	}

	/*
	 * Check if the fighter's current target is within the attack range of the 
	 * fighter. Also checks if the target is available.
	 */
	public bool isTargetInRange ()
	{
		if (!isTargetAvailable ()) {
			return false;
		}
		bool result = Utils.getInstance ().isInRange (gameObject, target.gameObject, data.getAttackRange ());
		
		return result;
	}

	float getSpeed ()
	{
		return data.getSwiftness () / 4.0f;
	}

	/*
	 * Calculate and return time needed for one attack
	 */
	float getAttackTime ()
	{
		return 1f + (BASE_SWIFTNESS - data.getSwiftness ()) * 0.1f;
	}

	public void addListener (FighterListener listener)
	{
		data.addListener (listener);
	}

	/*
	 * Fighter is selected, when he is clicked on
	 */
	public void OnMouseDown ()
	{
		SelectionHandler.getInstance ().setSelection (new List<GameSelectable> {this});
	}

	#region GameSelectable implementation
	/*
	 * Selecting a fighter means showing his name tag and highlighting his health
	 * display via the HudController
	 */
	public void select ()
	{
		transform.FindChild ("NameTag").gameObject.SetActive (true);
	}

	/*
	 * Unselecting a fighter means hiding his name tag and turning of the 
	 * highlighting of his health display via the HudController
	 */
	public void unselect ()
	{
		transform.FindChild ("NameTag").gameObject.SetActive (false);
	}
	#endregion

	/*
	 * Get first skill of the fighter.
	 */
	public ActiveSkill getFirstSkill ()
	{
		return activeSkills [0];
	}

	#region TimerOwner implementation
	/*
	 * Handled timers:
	 */
	private const string ATTACK_COOLDOWN_TIMER_ID = "attackCoolDownTimer";
	private const string RETREAT_TIMER_ID = "retreatTimer";

	public void timerExpired (string timerId)
	{
		if (timerId.Equals (ATTACK_COOLDOWN_TIMER_ID)) {
			// the fighter can attack again
			attackCoolDown = false;
		} else if (timerId.Equals (RETREAT_TIMER_ID)) {
			// fighter stops to retreat
			interrupt ();
			ignoreEnemies = false;
			setPriority (BehaviourKind.Retreat, 1);
		}
	}
	#endregion

	/*
	 * Get the priority of provided behaviour kind
	 */
	public int getPriority (BehaviourKind behaviour)
	{
		return priorities.getPriority (behaviour);
	}

	/*
	 * Set priority of provided behaviour kind
	 */
	public void setPriority (BehaviourKind behaviour, int priority)
	{
		priorities.setPriority (behaviour, priority);
	}

	/*
	 * Fighter stops doing whatever he was doing
	 */
	public void interrupt ()
	{
		stateMachine.stateToNoOp ();
		target = null;
	}


	/*
	 * Process the fighter's death - play animation, turn off the collider etc.
	 * This function must ONLY be called by the state machine! Don't use it anywhere else, please.
	 */
	public void die ()
	{
		data.setKnockedOut (true);
		GetComponent<Animation> ().Play ("die");
		target = null;
		GetComponent<CapsuleCollider> ().enabled = false;
	}

}


