using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*
 * Kinds of behaviour, that can be priorities by a fighter.
 */
public enum BehaviourKind
{
	None, // default
	Retreat, // the fighter will run away from enemies
	Aggression // the fighter will seek and attack enemies
}