using System;

namespace Fighters
{
/*
* States of the fighter. Depending on the state the fighter's behaviour is calculated.
* The state also defines the animation of the fighter.
*/ 
	public enum FighterActionState
	{
		NO_OP,
		MOVING,
		ATTACKING,
		OUT
	}
}