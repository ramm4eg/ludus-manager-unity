using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

/*
 * Skill, that makes the fighter retreat from danger.
 * It sets the priority of Retreat behaviour to 10 (currently considered maximum).
 */
class RetreatSkill : ActiveSkill
{
	private FighterController fighter;

	public RetreatSkill(FighterController f) {
		name = "Retreat";
		imageName = "retreat";
		fighter = f;
	}

	#region ActiveSkill implementation
	const int TOP_PRIORITY = 10;

	public override void execute(){
		fighter.setPriority (BehaviourKind.Retreat, TOP_PRIORITY);
		fighter.interrupt ();
	}
	#endregion
}


