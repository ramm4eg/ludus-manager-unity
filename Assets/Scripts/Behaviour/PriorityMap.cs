using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

/*
 * This class is the map of different types of fighter behaviour and their 
 * priorities. Each fighter has such a map and it determines fighter's behaviour 
 * in a fight. Each kind of behaviour has a priority, which is an integer. 
 * Minimal value is 0, maximal - maxInt. The higher the priority, the sooner will
 * the fighter take the respective behaviour. 
 * Different kinds of behaviour can have the same priority. So the mapping 
 * relation between behaviour kinds and priorities is n to 1.
 */
public class PriorityMap
{

	private Dictionary<BehaviourKind, int> kinds;
	private Dictionary<int, List<BehaviourKind>> priorities;

	/*
	 * Constructor. All priorities are set to default value of 0.
	 */
	public PriorityMap() {
		// instantiate for all behaviour kinds with default priorities
		kinds = new Dictionary<BehaviourKind, int> ();
		priorities = new Dictionary<int, List<BehaviourKind>>();
		setPriority (BehaviourKind.Retreat, 0);
		setPriority (BehaviourKind.Aggression, 0);
	}

	/*
	 * Change priority of behaviour kind.
	 * Priority is limited to 0 from below. 
	 */
	public void setPriority (BehaviourKind kind, int priority)
	{
		if (priority < 0)
			priority = 0;

		// remove mapping to old priority
		int oldPriority = 0;
		bool found = kinds.TryGetValue(kind, out oldPriority);
		if (found) {
			List<BehaviourKind> oldList = priorities [oldPriority];
			if (oldList != null) {
				oldList.Remove (kind);
				if (oldList.Count < 1) {
					priorities.Remove (oldPriority);
				}
			}
		}

		// add mapping to new priority
		kinds [kind] = priority;
		List<BehaviourKind> list = null;
		found = priorities.TryGetValue(priority, out list);
		if (!found) {
			list = new List<BehaviourKind>();
			priorities.Add(priority, list);
		}
		list.Add (kind);

	}

	/*
	 * Get priority of the behaviour kind
	 */
	public int getPriority (BehaviourKind behaviour)
	{
		return kinds [behaviour];
	}

	/*
	 * Find the highest priority and return behaviour kinds, that are 
	 * prioritized most highly
	 */
	public List<BehaviourKind> getBehaviourWithHighestPriority ()
	{
		List<int> keys = new List<int>(priorities.Keys);
		int highestPriority = -1;
		foreach (int priority in keys) {
			if (priority > highestPriority) {
				highestPriority = priority;
			}
		}
		return priorities [highestPriority];
	}
}


