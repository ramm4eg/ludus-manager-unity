using System; 

namespace Fighters
{
/*
 * This class implements a state machine designed to keep track of fighter's actions.
 * The fighter starts in a NO_OP state, which is his idle state.
 * In the function act() the state machine tells the fighter how to act according to his current state.
 * In the function changeState() the state machine validates and performs a state transition.
 * Functions stateToNoOp() and stateToOut() change the state of the machine to NO_OP and OUT respectively; they
 * are called from outside the machine to interrupt fighter's actions or to notify machine about his death.
 */
	public class FighterStateMachine
	{

		FighterController fighter;
		private FighterActionState state = FighterActionState.NO_OP;

		/*
	 * Constructor
	 * 
	 * @param contr fighter controller for which this state machine is created
	 */
		public FighterStateMachine (FighterController contr)
		{
			fighter = contr;
		}

		/*
	 * Handle the current state of the machine. This method can be seen as
	 * "during" function of all states.
	 * 
	 * State:
	 * NO_OP - ask the fighter controller, what state should be next
	 * MOVING - tell fighter controller to move
	 * ATTACKING - tell fighter controller to attack
	 * OUT - do nothing (fighter is already knocked out)
	 */
		public void act ()
		{
			switch (state) {
			case FighterActionState.NO_OP:
			// find what to do next
				FighterActionState newState = fighter.startNextAction ();
				changeState (newState);
				break;
			case FighterActionState.MOVING:
				fighter.move ();
				break;
			case FighterActionState.ATTACKING:
				fighter.attack ();
				break;
			case FighterActionState.OUT:
				break;
			default:
				break;
			}
		}

		/*
	 * Make a transition in the fighter state machine.
	 * Check validity of transition, trigger on-exit and on-enter actions.
	 */
		private void changeState (FighterActionState newActionState)
		{
			//Debug.Log (state.ToString () + " changed to " + newActionState.ToString ());
		
			if (state == newActionState) {
				// this is not a change of state
				return;
			}
			if (state == FighterActionState.OUT) {
				return;
			}
		
			// check the validity of transition
			switch (newActionState) {
			case FighterActionState.NO_OP:
				break;
			case FighterActionState.MOVING:
				if (!fighter.isTargetAvailable ()) {
					changeState (FighterActionState.NO_OP);
					return;
				}
				break;
			case FighterActionState.ATTACKING:
				if (!fighter.isTargetInRange ()) {
					changeState (FighterActionState.NO_OP);
				}
				break;
			case FighterActionState.OUT:
				break;
			default:
				break;
			}
		
			// handle leaving the last state
			switch (state) {
			case FighterActionState.NO_OP:
				break;
			case FighterActionState.MOVING:
				break;
			case FighterActionState.ATTACKING:
				break;
			case FighterActionState.OUT:
				break;
			default:
				break;
			}
		
			switch (newActionState) {
			case FighterActionState.NO_OP:
			//Debug.Log("State changed to NO_OP");
				break;
			case FighterActionState.MOVING:
			//Debug.Log("State changed to MOVING");
				break;
			case FighterActionState.ATTACKING:
			//Debug.Log("State changed to ATTACKING");
				break;
			case FighterActionState.OUT:
			//Debug.Log("State changed to OUT");
				fighter.die ();
				break;
			default:
				break;
			}
		
			state = newActionState;
		}

		/*
	 * Return current state of the machine
	 */
		public FighterActionState getState ()
		{
			return state;
		}

		/*
	 * Change machine state to NO_OP (fighter will be idle and must start a new action)
	 */
		public void stateToNoOp ()
		{
			changeState (FighterActionState.NO_OP);
		}

		/*
	 * Change machine state to OUT (figter will be knocked out and will not start any new actions any more)
	 */
		public void stateToOut ()
		{
			changeState (FighterActionState.OUT);
		}
	}
}

