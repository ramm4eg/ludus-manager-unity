
using System;
using System.Collections.Generic;

/*
 * This class is a data model of a fighter.
 */
namespace Fighters
{
	public class Fighter
	{
		private Team team;
		public static int initOffense = 10;
		public static int initDefense = 10;
		public static int initSwiftness = 10;
		public int offense = initOffense;
		public int defense = initDefense;
		public int swiftness = initSwiftness;
		private float attackRange = 0.6f;
		public string fighterName;
		private List<FighterListener> listeners = new List<FighterListener> ();
		private bool knockedOut = false;

		/*
	 * Constructor
	 * 
	 * @param fName fighter name
	 * @param team1 team, to which the fighter belongs
	 */
		public Fighter (string fName, Team team1)
		{
			fighterName = fName;
			team = team1;
		}

		public int getOffense ()
		{
			return offense;
		}
	
		public void setOffense (int newValue)
		{
			int old = offense;
			offense = newValue;
			foreach (FighterListener listener in listeners) {
				listener.offenseChanged (old, offense);
			}
		}
	
		public int getDefense ()
		{
			return defense;
		}
	
		public void setDefense (int newValue)
		{
			int old = defense;
			defense = newValue;
			foreach (FighterListener listener in listeners) {
				listener.defenseChanged (old, defense);
			}
		}
	
		public int getSwiftness ()
		{
			return swiftness;
		}
	
		public void setSwiftness (int newValue)
		{
			int old = swiftness;
			swiftness = newValue;
			foreach (FighterListener listener in listeners) {
				listener.swiftnessChanged (old, swiftness);
			}
		}

		public float getAttackRange ()
		{
			return attackRange;
		}

		/*
	 * Set fighter knocked out or back to life.
	 * 
	 * @param ko if true, the fighter will be considered dead
	 */
		public void setKnockedOut (bool ko)
		{
			knockedOut = ko;
			if (ko == true) {
				// notify listeners
				foreach (FighterListener listener in listeners) {
					listener.knockedOut ();
				}
			}
		}

		/*
	 * Is the fighter dead / maimed / unconscious?
	 */
		public bool isKnockedOut ()
		{
			return knockedOut;
		}

		public Team getTeam ()
		{
			return team;
		}

		public void setTeam (Team team1)
		{
			team = team1;
		}
	
		public string getName ()
		{
			return fighterName;
		}
	
		public void setName (string fName)
		{
			fighterName = fName;
		}

		public void addListener (FighterListener listener)
		{
			listeners.Add (listener);
		}
	}
}

