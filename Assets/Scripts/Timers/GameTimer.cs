using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

/*
 * A time ticker. This class is intended to only be used by TimerUtil.
 */
class GameTimer
{
	float time = 0;
	string id;

	/*
	 * Constructor
	 */
	public GameTimer (string timerId)
	{
		id = timerId;
	}

	/*
	 * Update this timer
	 * @return true, if the timer has reached the threshold
	 */
	public bool update (float fixedDeltaTime)
	{
		time -= fixedDeltaTime;
		if (time <= 0)
			return true;
		else
			return false;
	}

	/*
	 * Get ID of the timer.
	 */
	public string getId ()
	{
		return id;
	}

	/*
	 * Set time to count down
	 */
	public void setTime (float t)
	{
		time = t;
	}
}



