using UnityEngine;
using System;

/*
 * Utilities to work with unity
 */
public class Utils : MonoBehaviour
{
	public const string RETREAT_POINT_TAG = "RetreatPoint";
	private static Utils instance;

	/*
	 * Initialize singleton
	 */
	void Start ()
	{
		if (instance != null) {
			Destroy (gameObject);
		}
		if (instance == null) {
			instance = this;
		}
	}

	/*
	 * Singleton
	 */
	public static Utils getInstance ()
	{
		return instance;
	}

	/*
	 * Find a fighter, that is in oposing team, than the provided one, and is
	 * nearest to him.
	 * 
	 * @param self fighter, who's enemy we are looking for
	 * @return nearest available enemy to "self"
	 */
	public GameObject findNearestEnemy (FighterController self)
	{
		GameObject result = null;
		float minDistance = -1f;

		GameObject[] fighters = GameObject.FindGameObjectsWithTag ("Fighter");
		foreach (GameObject fighter in fighters) {
			if (isTargetAvailable (fighter)) {
				FighterController contr = fighter.GetComponent<FighterController> ();
				if (!isSameTeam (self, contr)) {
					float distance = Vector3.Distance (self.transform.position, fighter.transform.position);

					if (minDistance > distance || minDistance < 0f) {
						result = fighter;
						minDistance = distance;
					}
				}
			}

		}

		return result;
	}

	/*
	 * Are the two fighters from the same team?
	 */
	public bool isSameTeam (FighterController one, FighterController two)
	{
		Team oneTeam = one.getTeam ();
		Team twoTeam = two.getTeam ();
		return oneTeam == twoTeam;
	}

	/*
	 * Is target a valid target for a fighter to pursuit?
	 * Points of interest (like Retreat points) are always valid.
	 * Fighters are only valid, if they are not out.
	 */
	public bool isTargetAvailable (GameObject target)
	{
		if (target == null) {
			return false;
		}

		// special targets
		if (target.tag.Equals (RETREAT_POINT_TAG)) {
			return true;
		}

		FighterController contr = target.GetComponent<FighterController> ();

		if (contr == null) {
			return false;
		}
		if (contr.isOut ()) {
			return false;
		}
		return true;
	}

	/*
	 * Is the distance between the objects less than provided range?
	 */
	public bool isInRange (GameObject obj1, GameObject obj2, float range)
	{
		return (Vector3.Distance (obj1.transform.position, obj2.transform.position) <= range);
	}

	/*
	 * Find a retreat point game object, that is nearest to the provided fighter.
	 * 
	 * @param self fighter
	 */
	public GameObject findNearestRetreatPoint (FighterController self)
	{
		Vector3 ownPos = self.transform.position;
		
		GameObject result = null;
		float minDistance = -1f;
		
		GameObject[] points = GameObject.FindGameObjectsWithTag (RETREAT_POINT_TAG);
		foreach (GameObject point in points) {
			Vector3 pos = point.transform.position;
			float sqrDistance = (ownPos - pos).sqrMagnitude;
			
			if (minDistance < sqrDistance || minDistance < 0) {
				result = point;
				minDistance = sqrDistance;
			}
			
		}
		
		return result;
	}
}

