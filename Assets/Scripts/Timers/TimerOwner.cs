using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

/*
 * An owner of timers in timer utility
 */
public interface TimerOwner
{
	/*
	 * Is called by timer util, when one of the timers, set by this timer owner,
	 * expires. Please, use this method to impelement your handling of the timer
	 * expiration.
	 * 
	 * @param timerId ID of the timer, that has expired
	 */
	void timerExpired (string timerId);
}



