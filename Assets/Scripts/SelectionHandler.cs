using System;
using System.Collections.Generic;
using UnityEngine;

/*
 * Utility, that handles and holds in-game unit selection.
 */
public class SelectionHandler
{

	private static SelectionHandler instance;
	private IList<SelectionListener> listeners;

	/*
	 * Singleton
	 */
	public static SelectionHandler getInstance ()
	{
		if (instance == null) {
			instance = new SelectionHandler ();
		}
		return instance;
	}

	private List<GameSelectable> currentSelection;

	/*
	 * Hidden constructor
	 */
	private SelectionHandler ()
	{
		currentSelection = new List<GameSelectable> ();
		listeners = new List<SelectionListener> ();
	}

	/*
	 * Set the list of objects as current selection. Cancel the previous 
	 * selection. Notify listeners.
	 * 
	 * @param selected list of objects to select
	 */
	public void setSelection (List<GameSelectable> selected)
	{
		unselectAll ();
		foreach (GameSelectable obj in selected) {
			obj.select ();
			currentSelection.Add (obj);
		}
		notifyListeners ();
	}

	/*
	 * Tell all currently selected objects that they are not selected any more.
	 * Clear current selection.
	 */
	private void unselectAll ()
	{
		foreach (GameSelectable selected in currentSelection) {
			selected.unselect ();
		}
		currentSelection.Clear ();
	}

	/*
	 * Tell all registered selection listeners, that selection has changed
	 */
	private void notifyListeners ()
	{
		foreach (SelectionListener listener in listeners) {
			listener.selectionChanged (currentSelection);
		}
	}

	/*
	 * Register a selection listener
	 */
	public void addSelectionListener (SelectionListener listener)
	{
		listeners.Add (listener);
	}
}

